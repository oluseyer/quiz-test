import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    session: JSON.parse(localStorage.getItem("session")) || null,
    answers: JSON.parse(localStorage.getItem("answers")) || null,
  },
  mutations: {
    SET_SESSION: (state, payload) => (state.session = payload),
    SET_ANSWERS: (state, payload) => (state.answers = payload),
  },
  actions: {
    saveSession(context, payload) {
      // Persist session in localstorage to handle browser reload
      localStorage.setItem("session", JSON.stringify(payload));
      context.commit("SET_SESSION", payload);
    },
    saveAnswer(context, { questionIndex, answerId }) {
      // Persist session in localstorage to handle browser reload
      let answers = JSON.parse(localStorage.getItem("answers")) || [];
      answers[questionIndex] = answerId;
      localStorage.setItem("answers", JSON.stringify(answers));
      context.commit("SET_ANSWERS", answers);
    },
    saveResult(context, payload) {
      const session = context.state.session;
      session.result = payload;
      localStorage.setItem("session", JSON.stringify(session));
      context.commit("SET_SESSION", session);
    },
    resetSession(context) {
      context.commit("SET_SESSION", null);
      context.commit("SET_ANSWERS", null);
      localStorage.removeItem("session");
      localStorage.removeItem("answers");
    },
  },
  modules: {},
});
