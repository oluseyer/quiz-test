import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Quiz from "@/views/Quiz";
import Result from "@/views/Result";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/quiz/:id",
    name: "Quiz",
    component: Quiz,
  },
  {
    path: "/result",
    name: "Result",
    component: Result,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
