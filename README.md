# quiz-test

## Project architecture
```
This project makes uses of vue dynamic routing to reuse the Quiz.vue page component.
It also uses Vuex for state management and Local Storage for data persistence. 
```


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
